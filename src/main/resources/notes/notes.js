define('plugin/git-notes', ['jquery', 'aui', 'exports'], function ($, AJS, exports) {
    exports.onReady = function (buttonSelector, contentSelector) {
        var $button = $(buttonSelector);
        var $dialogContent = $(contentSelector);

        var dialog = AJS.InlineDialog($button, "git-notes-dialog", function (content, trigger, showPopup) {
            content.append($dialogContent);
            showPopup();
        });

        $(document).keyup(function(e) {
            if(e.keyCode === 27) {
                dialog.hide();
            }
        });

        $dialogContent
            .find("a.close-link")
            .on('click', function (e) {
                dialog.hide();
                e.preventDefault();
            });
    }
});
