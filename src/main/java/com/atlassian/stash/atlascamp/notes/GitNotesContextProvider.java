package com.atlassian.stash.atlascamp.notes;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.repository.Repository;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Provides the context for rendering the {@code git-notes-changeset-panel} web-panel defined in
 * {@code atlassian-plugin.xml}
 */
public class GitNotesContextProvider implements ContextProvider {

    private final GitNotesService gitNotesService;

    public GitNotesContextProvider(GitNotesService gitNotesService) {
        this.gitNotesService = gitNotesService;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
        // no params
    }

    /**
     * Attempt to resolve a git note from the context repository and changeset.
     *
     * TODO gitNotesService should be queried on an async request from the client, rather than on the page request thread
     */
    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        Repository repository = (Repository) context.get("repository");
        Changeset changeset = (Changeset) context.get("changeset");

        Map<String, Object> panelContext = Maps.newHashMap(context);
        panelContext.put("note", gitNotesService.getNoteFor(repository, changeset));
        return panelContext;
    }

}
