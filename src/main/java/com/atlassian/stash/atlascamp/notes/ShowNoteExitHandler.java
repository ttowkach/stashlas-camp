package com.atlassian.stash.atlascamp.notes;

import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.scm.DefaultCommandExitHandler;

/**
 * Exit handler that stops a non-zero exit code from 'git notes show' from being considered an error if the error
 * message indicates that no note is defined for the supplied commit.
 */
public class ShowNoteExitHandler extends DefaultCommandExitHandler {

    public ShowNoteExitHandler(I18nService i18nService) {
        super(i18nService);
    }

    @Override
    protected boolean isError(String command, int exitCode, String stdErr, Throwable thrown) {
        return !stdErr.startsWith("error: No note found for object") && super.isError(command, exitCode, stdErr, thrown);
    }

}
