package com.atlassian.stash.atlascamp.notes;

import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.ScmType;
import com.atlassian.stash.scm.Command;
import com.atlassian.stash.scm.ScmClient;
import com.atlassian.stash.scm.ScmClientProvider;
import com.atlassian.stash.throttle.ThrottleService;

/**
 * Simple service that retrieves git notes associated with particular changesets.
 */
public class GitNotesService {

    private final ScmClientProvider scmClientProvider;
    private final I18nService i18nService;

    public GitNotesService(ScmClientProvider scmClientProvider, I18nService i18nService) {
        this.scmClientProvider = scmClientProvider;
        this.i18nService = i18nService;
    }

    /**
     * Retrieve a git note for a changeset.
     *
     * TODO use the {@link ThrottleService} to acquire a ticket for the Git operation
     *
     * @param repository the context repository for a changeset (necessary as the same commit may exist in multiple
     *                   repositories. For example this will occur when Stash is hosting multiple forks of the same
     *                   repository.
     * @param changeset a changeset that may or may not have a git note associated with it
     * @return the text from a git note for the changeset if there is a git note associated with it, or null otherwise
     */
    public String getNoteFor(Repository repository, Changeset changeset) {

        /**
         * Get an instance of ScmClient and check what type of repository it is. Bail out if the context repository is
         * not a Git repository (though in practice in stock versions of Stash, it will be).
         */
        ScmClient scmClient = scmClientProvider.get(repository);
        if (!scmClient.getScmType().equals(ScmType.GIT)) {
            return null;
        }

        String note = null;

        /**
         * Create a command for invoking 'git notes show <changesetId>'
         */
        Command<String> command = scmClient
                        .builder(repository)
                        .command("notes")
                        .argument("show")
                        .argument(changeset.getId())
                        .exitHandler(new ShowNoteExitHandler(i18nService))
                        .build(new ShowNoteOutputHandler());

        /**
         * Execute the command and return the content of the note.
         */
        return command.call();
    }

}
