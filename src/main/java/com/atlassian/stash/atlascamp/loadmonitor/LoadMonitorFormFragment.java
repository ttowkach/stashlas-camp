package com.atlassian.stash.atlascamp.loadmonitor;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.mail.MailService;
import com.atlassian.stash.ui.FormFragment;
import com.atlassian.stash.ui.ValidationErrors;
import com.atlassian.stash.view.TemplateRenderingException;
import com.google.common.collect.Maps;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * {@link FormFragment} for configuring the email address for dispatching overload notifications to. This fragment
 * is bound to the Stash Server Settings administration form.
 */
public class LoadMonitorFormFragment implements FormFragment {

    public static final String LOAD_MONITOR_EMAIL = "loadMonitorEmail";

    private static final String NO_MAIL_SERVER = "noMailServer";
    private static final String SOY_RESOURCE_KEY = "com.atlassian.stash.stash-atlascamp-showcase:load-monitor-resources";
    private static final String CONFIG_TEMPLATE = "stash.fragment.load.monitor.config";

    // Regex from http://www.regular-expressions.info/email.html
    private static final Pattern EMAIL_RX = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b", Pattern.CASE_INSENSITIVE);

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final MailService mailService;

    public LoadMonitorFormFragment(SoyTemplateRenderer soyTemplateRenderer, PluginSettingsFactory pluginSettingsFactory,
                                   MailService mailService) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.mailService = mailService;
    }

    @Override
    public void doView(Appendable appendable) throws IOException {
        Map<String, Object> context = Maps.newHashMap();
        context.put(NO_MAIL_SERVER, !mailService.isHostConfigured());
        context.put(LOAD_MONITOR_EMAIL, pluginSettingsFactory.createGlobalSettings().get(LOAD_MONITOR_EMAIL));
        render(appendable, CONFIG_TEMPLATE, context);
    }

    @Override
    public void validate(Map<String, String[]> requestParams, ValidationErrors validationErrors) {
        String email = getParamValue(LOAD_MONITOR_EMAIL, requestParams);
        if (email != null && !EMAIL_RX.matcher(email).matches()) {
            validationErrors.addFieldError(LOAD_MONITOR_EMAIL, "Not a valid email address.");
        }
    }

    @Override
    public void doError(Appendable appendable, Map<String, String[]> requestParams, Map<String, Collection<String>> fieldErrors) throws IOException {
        Map<String, Object> context = Maps.newHashMap();
        context.put(LOAD_MONITOR_EMAIL, getParamValue(LOAD_MONITOR_EMAIL, requestParams));
        context.put("errors", fieldErrors);
        render(appendable, CONFIG_TEMPLATE, context);
    }

    @Override
    public void execute(Map<String, String[]> requestParams) {
        PluginSettings globalSettings = pluginSettingsFactory.createGlobalSettings();
        String email = getParamValue(LOAD_MONITOR_EMAIL, requestParams);
        if (email == null) {
            globalSettings.remove(LOAD_MONITOR_EMAIL);
        } else {
            globalSettings.put(LOAD_MONITOR_EMAIL, email);
        }
    }

    private String getParamValue(String name, Map<String, String[]> requestParams) {
        String[] values = requestParams.get(name);
        if (values.length > 0 && values[0] != null && !values[0].trim().isEmpty()) {
            return values[0].trim();
        } else {
            return null;
        }
    }

    private void render(Appendable appendable, String template, Map<String, Object> context) {
        try {
            soyTemplateRenderer.render(appendable, SOY_RESOURCE_KEY, template, context);
        } catch (SoyException e) {
            throw new TemplateRenderingException("Failed to render " + template, e);
        }
    }

}
