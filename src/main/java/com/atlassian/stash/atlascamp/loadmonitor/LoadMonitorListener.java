package com.atlassian.stash.atlascamp.loadmonitor;

import com.atlassian.event.api.EventListener;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.stash.event.TicketRejectedEvent;
import com.atlassian.stash.mail.MailMessage;
import com.atlassian.stash.mail.MailService;
import com.atlassian.stash.server.ApplicationPropertiesService;

/**
 * Event listener that listens for {@link TicketRejectedEvent throttling events} and dispatches a warning message to
 * the configured email address.
 */
public class LoadMonitorListener {

    private final MailService mailService;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final PluginSettingsFactory pluginSettingsFactory;

    public LoadMonitorListener(MailService mailService, ApplicationPropertiesService applicationPropertiesService,
                               PluginSettingsFactory pluginSettingsFactory) {
        this.mailService = mailService;
        this.applicationPropertiesService = applicationPropertiesService;
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    /**
     * Easy-peasy event listening, no need to even register this class with an event publisher! Internally Stash has a
     * registrar that will track plugin components as they come online and register any event listeners for you.
     */
    @EventListener
    public void onThrottled(TicketRejectedEvent ticketRejectedEvent) {
        String subject = applicationPropertiesService.getDisplayName() + " overloaded!";

        String bodyText = String.format("The Stash server at %s is rejecting requests due to high load. \n\nResource: %s",
            applicationPropertiesService.getBaseUrl().toASCIIString(),
            ticketRejectedEvent.getResourceName()
        );

        sendEmail(subject, bodyText);
    }

    /**
     * Dispatches an email to the email address configured for receiving load alerts.
     *
     * @param subject the email subject
     * @param bodyText the body text of the email
     */
    private void sendEmail(String subject, String bodyText) {
        String recipient = (String) pluginSettingsFactory.createGlobalSettings().get(LoadMonitorFormFragment.LOAD_MONITOR_EMAIL);
        if (recipient == null || !mailService.isHostConfigured()) {
            return;
        }

        String serverEmail = applicationPropertiesService.getServerEmailAddress();
        if (serverEmail == null) {
            serverEmail = "noreply@example.com";
        }

        MailMessage notification = new MailMessage.Builder()
                .from(serverEmail)
                .to(recipient)
                .subject(subject)
                .text(bodyText)
                .build();

        mailService.sendMessage(notification);
    }

}
